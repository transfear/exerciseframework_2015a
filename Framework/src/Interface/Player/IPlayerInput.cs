﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrameworkInterface.Player
{
	public interface IPlayerInput
	{
		IPlayerInput CopyDeep();
	}
}
