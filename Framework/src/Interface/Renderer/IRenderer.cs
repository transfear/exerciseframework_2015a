﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FrameworkInterface.Renderer
{
	public interface IRenderer
	{
		IRendererFeedback OnGameStart(FrameworkInterface.Simulation.ISimulationOutput _simulationData);
		IRendererFeedback Update(FrameworkInterface.Simulation.ISimulationOutput _simulationData);
		IRendererFeedback OnGameEnd(FrameworkInterface.Simulation.ISimulationOutput _simulationData);
	}
}
