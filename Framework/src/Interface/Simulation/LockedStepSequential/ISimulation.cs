﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace FrameworkInterface.Simulation.LockedStepSequential
{
	public abstract class ISimulation : FrameworkInterface.Simulation.ISimulation
	{
		public override SimulationType_e GetSimulationType() { return SimulationType_e.kSimulationType_LockedStepSequential; }

		// If using kSimulationType_LockedStepSequential, determines in which order the players are updated
		public abstract ICollection<IPlayer> GetPlayerOrder(ICollection<IPlayer> _playerList);

		// Called once per player, each turn
		public abstract IPlayerInput      OnPlayerTurn_Start(IPlayer _playerToUpdate);
		public abstract ISimulationOutput OnPlayerTurn_End(IPlayer _updatedPlayer, IPlayerOutput _singlePlayerOutput);
	}
}
