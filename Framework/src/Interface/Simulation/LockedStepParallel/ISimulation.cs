﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace FrameworkInterface.Simulation.LockedStepParallel
{
	public abstract class ISimulation : FrameworkInterface.Simulation.ISimulation
	{
		public override SimulationType_e GetSimulationType() { return SimulationType_e.kSimulationType_LockedStepParallel; }

		// Called once per turn. The collection holds every player output.
		public abstract ICollection<IPlayerInput> OnPlayerTurn_Start(ICollection<IPlayer> _playerCollection);
		public abstract ISimulationOutput OnPlayerTurn_End(ICollection<Tuple<IPlayer, IPlayerOutput>> _playerOutputs);
	}
}
