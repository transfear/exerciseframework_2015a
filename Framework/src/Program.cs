﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using FrameworkInterface.Player;

namespace Framework
{
	class Program
	{
		static FrameworkInterface.Simulation.ISimulation      mSimulation = null;
		static FrameworkInterface.Renderer.IRenderer          mRenderer   = null;
		static IList<IPlayer> mPlayers    = null;

		static void Main(string[] args)
		{
			if (Init())
			{
				bool bNextGame = true;
				do
				{
					StartGame();
					RunGame();
					bNextGame = EndGame();
				}
				while (bNextGame);
			}
		}

		static bool Init()
		{
			// Look in current directory, and attempt to find DLLs where valid
			// assemblies could be loaded and used by the framework.

			string   strCurDirectory = Directory.GetCurrentDirectory();
			string[] strFiles        = Directory.GetFiles(strCurDirectory, "*.dll", SearchOption.AllDirectories);

			// Load all exported types
			List<Type> exportedTypeList = new List<Type>();
			foreach (string strCurDLL in strFiles)
			{
				string strFilename = Path.GetFileName(strCurDLL);

				try
				{
					Console.WriteLine("Trying to load " + strFilename);
					Assembly curLibrary = Assembly.LoadFile(strCurDLL);

					Type[] curLibTypes = curLibrary.GetExportedTypes();
					foreach (Type curType in curLibTypes)
					{
						// Ignore abstract classes
						if (curType.IsAbstract)
							continue;

						// Ignore interfaces
						if (curType.IsInterface)
							continue;

						exportedTypeList.Add(curType);
					}
				}
				catch (System.Exception ex)
				{
					Console.WriteLine("Failed to load " + strFilename + " because " + ex.Message);
				}
			}

			// Ask the instantiator to instantiate all types
			InstantiateTypes(exportedTypeList);
			
			// Validation
			if (mSimulation == null)
			{
				Console.WriteLine("No simulation could be found. Exiting.");
				return false;
			}

			if (mRenderer == null)
			{
				Console.WriteLine("No renderer could be found. Exiting.");
				return false;
			}

			if (mPlayers.Count == 0)
			{
				Console.WriteLine("No player could be found. Exiting.");
				return false;
			}

			return true;
		}

		static void InstantiateTypes(IEnumerable<Type> _typeList)
		{
			Type instantiatorType = typeof(FrameworkInterface.Simulation.ISimulationInstantiator);
			Type simulationType   = typeof(FrameworkInterface.Simulation.ISimulation);
			Type rendererType     = typeof(FrameworkInterface.Renderer.IRenderer);
			Type playerType       = typeof(FrameworkInterface.Player.IPlayer);

			// Find instantiator, and possible simulations, renderers and players candidates
			FrameworkInterface.Simulation.ISimulationInstantiator instantiator = null;
			List<Type> simulationTypeList = new List<Type>();
			List<Type> rendererTypeList   = new List<Type>();
			List<Type> playerTypeList     = new List<Type>();
			foreach (Type curType in _typeList)
			{
				// Is it an ISimulationInstantiator?
				if (instantiatorType.IsAssignableFrom(curType))
				{
					if (instantiator == null)
					{
						try
						{
							instantiator = (FrameworkInterface.Simulation.ISimulationInstantiator)Activator.CreateInstance(curType);
							Console.WriteLine("Using class " + curType.Name + " as instantiator.");
						}
						catch (System.Exception exCtor)
						{
							Console.WriteLine("Tried to use class " + curType.Name + " as instantiator, but following error occured: " + exCtor.Message);
						}
					}
					else
					{
						Console.WriteLine("Ignoring instatiator type " + curType.Name);
					}
				}

				// Is it an ISimulation?
				if (simulationType.IsAssignableFrom(curType))
					simulationTypeList.Add(curType);

				// Is it an IRenderer?
				if (rendererType.IsAssignableFrom(curType))
					rendererTypeList.Add(curType);

				// Is it an IPlayer?
				if (playerType.IsAssignableFrom(curType))
					playerTypeList.Add(curType);
			}
			
			// No instantiator found?
			if (instantiator == null)
			{
				Console.WriteLine("No instantiator found, exiting.");
				return;
			}

			// Instantiate all types
			mSimulation = instantiator.InstantiateSimulation(simulationTypeList);
			mRenderer   = instantiator.InstantiateRenderer(rendererTypeList);
			mPlayers    = new List<IPlayer>(instantiator.InstantiatePlayers(playerTypeList));
			
			// Player validation
			UInt32 uiCurPlayer = 0;
			foreach(IPlayer curPlayer in mPlayers)
			{
				curPlayer.SetPlayerIndex(uiCurPlayer);

				Type   curPlayerType = curPlayer.GetType();
				String szDLLPath     = curPlayerType.Assembly.Location;
				curPlayer.SetDLLPath(szDLLPath);
				
				++uiCurPlayer;
			}
		}

		static void StartGame()
		{
			// Start game simulation
			ICollection<IPlayer>      orderedPlayers = new List<IPlayer>(mPlayers);
			ICollection<IPlayerInput> startInputs    = mSimulation.OnGameStart_Begin(orderedPlayers);
			Debug.Assert(orderedPlayers.Count == startInputs.Count);

			// Notify players
			List<IPlayerOutput> playerOutputList = new List<IPlayerOutput>();
			var playerStartList = orderedPlayers.Zip(startInputs, (x, y) => new Tuple<IPlayer, IPlayerInput>(x, y));
			foreach (Tuple<IPlayer, IPlayerInput> curPlayer in playerStartList)
			{
				IPlayerInput playerInput = curPlayer.Item2;
				if (playerInput != null)
					playerInput = playerInput.CopyDeep();

				IPlayerOutput playerOutput = curPlayer.Item1.OnGameStart(playerInput);
				if (playerOutput != null)
					playerOutput = playerOutput.CopyDeep();

				playerOutputList.Add(playerOutput);
			}

			
			// Feed player output to simulation, and notify renderer
			FrameworkInterface.Simulation.ISimulationOutput simOutput = mSimulation.OnGameStart_End(playerOutputList);
			if (simOutput != null)
				simOutput = simOutput.CopyDeep();

			FrameworkInterface.Renderer.IRendererFeedback startFeedback = mRenderer.OnGameStart(simOutput);
			if (startFeedback != null)
				startFeedback = startFeedback.CopyDeep();

			mSimulation.ApplyRendererFeedback(startFeedback);
		}

		static void RunGame()
		{
			// Instantiate updater based on simulation type
			IUpdater updater = null;
			FrameworkInterface.Simulation.SimulationType_e eSimType = mSimulation.GetSimulationType();

			if (eSimType == FrameworkInterface.Simulation.SimulationType_e.kSimulationType_LockedStepSequential)
			{
				updater = new LockedStepSequentialUpdater();
			}
			else if (eSimType == FrameworkInterface.Simulation.SimulationType_e.kSimulationType_LockedStepParallel)
			{
				updater = new LockedStepParallelUpdater();
			}
			else if (eSimType == FrameworkInterface.Simulation.SimulationType_e.kSimulationType_RealTime)
			{
				updater = new RealTimeUpdater();
			}

			if (updater == null)
			{
				Console.WriteLine("Could not instantiate updater. Simulation return unsupported simulation type " + eSimType.ToString());
				return;
			}

			updater.Update(mSimulation, mRenderer, mPlayers);
		}

		static bool EndGame()
		{
			// End game simulation
			ICollection<IPlayer> orderedPlayers = new List<IPlayer>(mPlayers);
			ICollection<IPlayerInput> endInputs = mSimulation.OnGameEnd_Begin(orderedPlayers);
			Debug.Assert(orderedPlayers.Count == endInputs.Count);

			// Notify players
			List<IPlayerOutput> playerOutputList = new List<IPlayerOutput>();
			var playerEndList = orderedPlayers.Zip(endInputs, (x, y) => new Tuple<IPlayer, IPlayerInput>(x, y));
			foreach (Tuple<IPlayer, IPlayerInput> curPlayer in playerEndList)
			{
				IPlayerInput playerInput = curPlayer.Item2;
				if (playerInput != null)
					playerInput = playerInput.CopyDeep();

				IPlayerOutput playerOutput = curPlayer.Item1.OnGameEnd(playerInput);
				if (playerOutput != null)
					playerOutput = playerOutput.CopyDeep();

				playerOutputList.Add(playerOutput);
			}

			// Feed player output to simulation, and notify renderer
			FrameworkInterface.Simulation.ISimulationOutput simOutput = mSimulation.OnGameEnd_End(playerOutputList);
			if (simOutput != null)
				simOutput = simOutput.CopyDeep();

			FrameworkInterface.Renderer.IRendererFeedback endFeedback = mRenderer.OnGameEnd(simOutput);
			if (endFeedback != null)
				endFeedback = endFeedback.CopyDeep();

			mSimulation.ApplyRendererFeedback(endFeedback);

			return mSimulation.ShouldRestart();
		}
	}
}
