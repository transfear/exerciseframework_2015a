﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Simulation;
using FrameworkInterface.Player;
using FrameworkInterface.Renderer;

namespace Framework
{
	class LockedStepSequentialUpdater : IUpdater
	{
		FrameworkInterface.Simulation.LockedStepSequential.ISimulation mSimulation = null;
		IRenderer            mRenderer = null;
		ICollection<IPlayer> mPlayers  = null;

		public void Update(ISimulation _simulation, IRenderer _renderer, ICollection<IPlayer> _playerCollection)
		{
			mSimulation = _simulation as FrameworkInterface.Simulation.LockedStepSequential.ISimulation;
			mRenderer   = _renderer;
			mPlayers    = _playerCollection;

			// Turn by turn, until the game finishes
			bool bGameFinished = mSimulation.IsGameFinished();
			while (!bGameFinished)
			{
				// Process each player, sequentially
				ICollection<IPlayer> orderedPlayers = mSimulation.GetPlayerOrder(mPlayers);

				foreach (IPlayer curPlayer in orderedPlayers)
				{
					// Do player's turn
					IPlayerInput playerInput = mSimulation.OnPlayerTurn_Start(curPlayer);
					if (playerInput != null)
						playerInput = playerInput.CopyDeep();

					IPlayerOutput playerOutput = curPlayer.Update(playerInput);
					if (playerOutput != null)
						playerOutput = playerOutput.CopyDeep();

					ISimulationOutput simOutput = mSimulation.OnPlayerTurn_End(curPlayer, playerOutput);
					if (simOutput != null)
						simOutput = simOutput.CopyDeep();

					// Update renderer
					IRendererFeedback feedback = mRenderer.Update(simOutput);
					if (feedback != null)
						feedback = feedback.CopyDeep();

					mSimulation.ApplyRendererFeedback(feedback);
				}

				bGameFinished = mSimulation.IsGameFinished();
			}

			// Game finishes
		}
	}
}
