﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;

using Simulation_2015a.Data;

using FrameworkInterface.Player;
using FrameworkInterface.Renderer;
using FrameworkInterface.Simulation;

namespace Simulation_2015a.Internal
{
	class BaseSimulation
	{
		#region Public Interface
		public ICollection<IPlayer> PlayerList { get; set; }

		public void OnGameStart(ICollection<IPlayer> _playerCollection)
		{
			PlayerList = _playerCollection;
			Init();
		}

		public IPlayerInput ProducePlayerInput(IPlayer _player)
		{
			PlayerInput toReturn = new PlayerInput();

			int addedEnemies = 0;
			toReturn.enemies = new PlayerInput.EnemyPlayerData[PlayerList.Count - 1];

			// Loop on all players
			foreach (KeyValuePair<IPlayer, PlayerData> curPair in mPlayerMap)
			{
				IPlayer    curPlayer = curPair.Key;
				PlayerData curData   = curPair.Value;

				if (curPlayer == _player)
				{
					// Add data for current player
					toReturn.playerData.playerIdx = curPlayer.PlayerIndex;
					toReturn.playerData.pos       = curData.curPos.ToVector2();
					toReturn.playerData.home      = curData.home.ToVector2();
					toReturn.playerData.obj       = curData.obj;

					toReturn.playerData.accumulatedPostIts = curData.carryingPostIts.ToArray();
				}
				else
				{
					// Add data for enemies
					toReturn.enemies[addedEnemies].playerIdx = curPlayer.PlayerIndex;
					toReturn.enemies[addedEnemies].pos       = curData.curPos.ToVector2();
					toReturn.enemies[addedEnemies].home      = curData.home.ToVector2();
					toReturn.enemies[addedEnemies].obj       = curData.obj;
					++addedEnemies;
				}
			}

			toReturn.wantedObj = mObjective;
			toReturn.postIts   = mAvailablePostIts.ToArray();
			
			toReturn.remainingPostIts    = mRemainingPostItsToSpawn;
			toReturn.gridWidth           = mGridWidth;
			toReturn.gridHeight          = mGridHeight;
			toReturn.maxCarriablePostIts = mMaxCarryablePostIts;

			return toReturn;
		}

		#region ApplyPlayerOutput
		public void ApplyPlayerOuput(IPlayer _player, IPlayerOutput _playerResponse)
		{
			PlayerOutput response = _playerResponse as PlayerOutput;
			if (response == null)
				return;

			PlayerData          playerData = mPlayerMap[_player];
			PlayerOutput.Action toDo       = response.action;
			if (toDo == null)
				return;

			if (toDo.typeOfAction == PlayerOutput.Action.ActionType.Move)
			{
				PlayerOutput.MoveAction moveAction = (PlayerOutput.MoveAction)toDo;
				ApplyPlayerOutput_Move(playerData, moveAction);
			}
			else if (toDo.typeOfAction == PlayerOutput.Action.ActionType.PlacePostIt)
			{
				PlayerOutput.PlacePostItAction placePostIt = (PlayerOutput.PlacePostItAction)toDo;
				ApplyPlayerOutput_PlacePostIt(playerData, placePostIt);
			}
			else if (toDo.typeOfAction == PlayerOutput.Action.ActionType.MovePostIt)
			{
				PlayerOutput.MovePostItAction movePostIt = (PlayerOutput.MovePostItAction)toDo;
				ApplyPlayerOutput_MovePostIt(playerData, movePostIt);
			}
			else if (toDo.typeOfAction == PlayerOutput.Action.ActionType.DestroyPostIt)
			{
				PlayerOutput.DestroyPostItAction destroyPostIt = (PlayerOutput.DestroyPostItAction)toDo;
				ApplyPlayerOutput_DestroyPostIt(playerData, destroyPostIt);
			}
		}

		private void ApplyPlayerOutput_Move(PlayerData _playerData, PlayerOutput.MoveAction _moveAction)
		{
			if (_moveAction.Move == PlayerOutput.MoveAction.Movement_e.Up && _playerData.curPos.y > 0)
				--_playerData.curPos.y;
			else if (_moveAction.Move == PlayerOutput.MoveAction.Movement_e.Down && _playerData.curPos.y < (mGridHeight - 1))
				++_playerData.curPos.y;
			else if (_moveAction.Move == PlayerOutput.MoveAction.Movement_e.Left && _playerData.curPos.x > 0)
				--_playerData.curPos.x;
			else if (_moveAction.Move == PlayerOutput.MoveAction.Movement_e.Right && _playerData.curPos.x < (mGridWidth - 1))
				++_playerData.curPos.x;
		}

		private void ApplyPlayerOutput_PlacePostIt(PlayerData _playerData, PlayerOutput.PlacePostItAction _placePostIt)
		{
			// Ignore this move if player is not at home
			if ((_playerData.curPos.x != _playerData.home.x) || (_playerData.curPos.y != _playerData.home.y))
				return;
			
			// Ignore this move if player asked to place an invalid post-it
			int postItIdx = (int)_placePostIt.postItIdx;
			if (postItIdx >= _playerData.carryingPostIts.Count)
				return;
			
			// Ignore this move if player specified an invalid location at which to place the post-it
			UInt32 posX = _placePostIt.posX;
			UInt32 posY = _placePostIt.posY;
			if ((posX >= _playerData.obj.pixels.GetLength(0)) || (posY >= _playerData.obj.pixels.GetLength(1)))
				return;

			_playerData.obj.pixels[posX, posY] = _playerData.carryingPostIts.ElementAt(postItIdx);
			_playerData.carryingPostIts.RemoveAt(postItIdx);
		}

		private void ApplyPlayerOutput_MovePostIt(PlayerData _playerData, PlayerOutput.MovePostItAction _movePostIt)
		{
			// Ignore this move if player is not at home
			if ((_playerData.curPos.x != _playerData.home.x) || (_playerData.curPos.y != _playerData.home.y))
				return;

			int maxX = _playerData.obj.pixels.GetLength(0);
			int maxY = _playerData.obj.pixels.GetLength(1);

			UInt32 srcX = _movePostIt.srcX;
			UInt32 srcY = _movePostIt.srcY;
			UInt32 dstX = _movePostIt.dstX;
			UInt32 dstY = _movePostIt.dstY;
			
			// Ignore this move if player provided out-of-bound coordinates
			if ((srcX >= maxX) || (dstX >= maxX) || (srcY >= maxY) || (dstY >= maxY))
				return;

			_playerData.obj.pixels[dstX, dstY] = _playerData.obj.pixels[srcX, srcY];
			_playerData.obj.pixels[srcX, srcY] = new Data.Color();
		}

		private void ApplyPlayerOutput_DestroyPostIt(PlayerData _playerData, PlayerOutput.DestroyPostItAction _destroyPostIt)
		{
			// Ignore this move if player asked to place an invalid post-it
			int postItIdx = (int)_destroyPostIt.postItIdx;
			if (postItIdx >= _playerData.carryingPostIts.Count)
				return;

			_playerData.carryingPostIts.RemoveAt(postItIdx);
		}

		#endregion

		public ISimulationOutput ProduceSimulationOutput()
		{
			UpdateTurn();

			SimulationOutput toReturn = new SimulationOutput();

			int numPlayers = mPlayerMap.Count;
			toReturn.players = new SimulationOutput.PlayerData[numPlayers];

			// Fill all players
			int addedPlayer = 0;
			foreach (KeyValuePair<IPlayer, PlayerData> curPair in mPlayerMap)
			{
				IPlayer    curPlayer = curPair.Key;
				PlayerData curData   = curPair.Value;

				SimulationOutput.PlayerData newData = new SimulationOutput.PlayerData();
				newData.player = curPlayer;
				newData.score  = curData.score;
				newData.curPos = curData.curPos;
				newData.home   = curData.home;
				newData.obj    = curData.obj;

				newData.objChanged          = curData.objChanged;
				newData.carryingPostItCount = curData.carryingPostIts.Count;
				toReturn.players[addedPlayer++] = newData;
			}

			toReturn.wantedObj = mObjective;
			toReturn.postIts   = mAvailablePostIts.ToArray();

			toReturn.gridDim.x = (int)mGridWidth;
			toReturn.gridDim.y = (int)mGridHeight;

			toReturn.maxCarriablePostIts = mMaxCarryablePostIts;
			toReturn.remainingPostIts    = mRemainingPostItsToSpawn;
			
			return toReturn;
		}

		public void ApplyRendererFeedback(IRendererFeedback _rendererFeedback)
		{
			RendererFeedback feedback = _rendererFeedback as RendererFeedback;
			if (feedback == null)
				return;

			// Just check if the renderer wants us to quit
			if (feedback.ShouldQuit)
			{
				mIsGameFinished  = true;
				mbShouldRestart = feedback.ShouldRestart;
			}
		}

		public void OnGameEnd()
		{
			// Nothing to do here for now
		}

		public bool IsGameFinished()
		{
			return mIsGameFinished;
		}

		public bool ShouldRestart()
		{
			return mbShouldRestart;
		}

		public TimeSpan GetFrequency()
		{
			return mFrequency;
		}
		#endregion


		#region Private Interface

		class PlayerData
		{
			public float score = 0.0f;

			public Vector2Int curPos;
			public Vector2Int home;

			public List<Data.Color> carryingPostIts = new List<Data.Color>();

			public Objective obj;
			public bool      objChanged = false;
		}

		bool mIsGameFinished  = false;
		bool mbShouldRestart = false;

		Dictionary<IPlayer, PlayerData> mPlayerMap = new Dictionary<IPlayer, PlayerData>();
		
		Objective mObjective;

		List<AvailablePostIt> mAvailablePostIts = new List<AvailablePostIt>();
		UInt32 mRemainingPostItsToSpawn = 0;

		// Parameters - default values
		UInt32   mGridWidth           = 35;
		UInt32   mGridHeight          = 17;
		UInt32   mMaxCarryablePostIts = 10;
		TimeSpan mFrequency           = new TimeSpan(0, 0, 0, 0, 33);	// 33 ms per frame

		Random mRNG = new Random();
		
		void Init()
		{
			mIsGameFinished = false;

			InitParameters();
			InitializeObjective();
			
			// Determine how many post-its to spawn (between 2 and 3 times the minimum amount necessary)
			int objWidth  = mObjective.pixels.GetLength(0);
			int objHeight = mObjective.pixels.GetLength(1);
			mRemainingPostItsToSpawn = (UInt32)(((double)objWidth * objHeight * PlayerList.Count) * (mRNG.NextDouble() + 1.0));
			
			mPlayerMap.Clear();
			mAvailablePostIts.Clear();

			// Fill player map
			foreach (IPlayer curPlayer in PlayerList)
			{
				PlayerData newData = new PlayerData();
				newData.score = 0.0f;
				
				newData.home.x = mRNG.Next(0, (int)mGridWidth);
				newData.home.y = mRNG.Next(0, (int)mGridHeight);

				newData.curPos = newData.home;

				newData.carryingPostIts.Clear();
				newData.obj.pixels = new Data.Color[objWidth, objHeight];
				newData.objChanged = true;

				mPlayerMap[curPlayer] = newData;
			}
		}

		void InitializeObjective()
		{
			// Fetch all files in our objectives folder
			string   objectiveFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "objectives");
			string[] fileList        = Directory.GetFiles(objectiveFolder, "*.*").Where(s => 
				s.EndsWith(".png") || s.EndsWith(".jpeg") || s.EndsWith(".jpg") || s.EndsWith(".gif") || s.EndsWith(".bmp")
			).ToArray();

			// Select a random file from it
			Int32  numFiles   = fileList.GetLength(0);
			Int32  fileIndex  = mRNG.Next(0, numFiles);
			string fileToOpen = fileList[fileIndex];

			// Decode .gif files
			BitmapImage bi = new BitmapImage();

			if (fileToOpen.EndsWith(".gif", StringComparison.InvariantCultureIgnoreCase))
			{
				using (Stream imageStreamSource = new FileStream(fileToOpen, FileMode.Open, FileAccess.Read, FileShare.Read))
				{
					GifBitmapDecoder decoder = new GifBitmapDecoder(imageStreamSource, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
					BitmapSource bitmapSource = decoder.Frames[0];

					JpegBitmapEncoder encoder = new JpegBitmapEncoder();
					MemoryStream memoryStream = new MemoryStream();

					encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
					encoder.Save(memoryStream);

					bi.BeginInit();
					bi.StreamSource = new MemoryStream(memoryStream.ToArray());
					bi.EndInit();

					memoryStream.Close();
				}
			}
			else
			{
				bi.BeginInit();
				bi.UriSource = new Uri(fileToOpen, UriKind.Absolute);
				bi.EndInit();
			}

			// Convert to 96 DPI
			double   dpi  = 96.0;
			int imgWidth  = bi.PixelWidth;
			int imgHeight = bi.PixelHeight;

			int iStride = imgWidth * bi.Format.BitsPerPixel;
			byte[] pixelData = new byte[iStride * imgHeight];
			bi.CopyPixels(pixelData, iStride, 0);

			BitmapSource bs = BitmapSource.Create(imgWidth, imgHeight, dpi, dpi, bi.Format, null, pixelData, iStride);
			
			// Copy to our objective internal color array
			if (bs.Format != PixelFormats.Rgb24)
				bs = new FormatConvertedBitmap(bs, PixelFormats.Rgb24, null, 0);

			mObjective.pixels = new Data.Color[imgWidth, imgHeight];

			int numBytes = imgWidth * imgHeight * 3;
			byte[] tmpBuf = new byte[numBytes];
			bs.CopyPixels(tmpBuf, imgWidth * 3, 0);

			int curByte = 0;
			for (int curCol = 0; curCol < imgHeight; ++curCol)
			{
				for (int curRow = 0; curRow < imgWidth; ++curRow)
				{
					mObjective.pixels[curRow, curCol] = new Data.Color
					{
						R = tmpBuf[curByte++],
						G = tmpBuf[curByte++],
						B = tmpBuf[curByte++]
					};
				}
			}
		}

		void InitParameters()
		{
			string parameterFile = "Parameters.xml";

			if (!File.Exists(parameterFile))
				return;

			try
			{
				XElement root = XElement.Load(parameterFile);

				XElement gridWidthNode = root.Descendants("GridWidth").FirstOrDefault();
				UInt32.TryParse(gridWidthNode.Value, out mGridWidth);

				XElement gridHeightNode = root.Descendants("GridHeight").FirstOrDefault();
				UInt32.TryParse(gridHeightNode.Value, out mGridHeight);

				XElement maxPostItsNode = root.Descendants("MaxPostIts").FirstOrDefault();
				UInt32.TryParse(maxPostItsNode.Value, out mMaxCarryablePostIts);

				int msFreq = 0;
				XElement timeSpanNode = root.Descendants("TimeSpan").FirstOrDefault();
				if (int.TryParse(timeSpanNode.Value, out msFreq))
					mFrequency = new TimeSpan(0, 0, 0, 0, msFreq);
			}
			catch (Exception e)
			{
				Debug.WriteLine("Failed to load XML param file: " + e.Message);
			}
		}

		#region Update Turn
		void UpdateTurn()
		{
			if (mIsGameFinished)
				return;

			// Update everyone's score
			UpdateScores();
			
			// Check if game has ended (post it count reached 0, or someone got 100.0%)
			CheckIfGameFinished();
			
			// Check if players can pick up post-its
			PickUpPostIts();
			
			// Check if we need to spawn new post-its
			if (!mIsGameFinished && NeedNewPostIt())
			{
				SpawnNewPostIt();
				--mRemainingPostItsToSpawn;
			}
		}

		void UpdateScores()
		{
			int objWidth  = mObjective.pixels.GetLength(0);
			int objHeight = mObjective.pixels.GetLength(1);

			UInt64 worstScore = (UInt64)objWidth * (UInt64)objHeight * 255 * 3;

			foreach (PlayerData curData in mPlayerMap.Values)
			{
				UInt64 curScore = 0;
				for (int curX = 0; curX < objWidth; ++curX)
				{
					for (int curY = 0; curY < objHeight; ++curY)
					{
						Data.Color playerColor = curData.obj.pixels[curX, curY];
						Data.Color objColor    = mObjective.pixels[curX, curY];

						int deltaR = Math.Abs(playerColor.R - objColor.R);
						int deltaG = Math.Abs(playerColor.G - objColor.G);
						int deltaB = Math.Abs(playerColor.B - objColor.B);
						int totalDelta = deltaR + deltaG + deltaB;

						curScore += (UInt64)totalDelta;
					}
				}
				
				double fracScore = (double)curScore / (double)worstScore;
				curData.score    = (1.0f - (float)fracScore) * 100.0f;
			}
		}

		void CheckIfGameFinished()
		{
			if (mRemainingPostItsToSpawn == 0)
			{
				mIsGameFinished = true;
				return;
			}

			foreach (PlayerData curData in mPlayerMap.Values)
			{
				if (curData.score == 100.0f)
				{
					mIsGameFinished = true;
					return;
				}
			}
		}

		void PickUpPostIts()
		{
			int numPostIts = mAvailablePostIts.Count;
			for (int curPostItIdx = 0; curPostItIdx < numPostIts; ++curPostItIdx)
			{
				bool pickedUp = false;

				AvailablePostIt curPostIt = mAvailablePostIts.ElementAt(curPostItIdx);
				Vector2Int curPos = curPostIt.pos.ToVector2Int();

				foreach (PlayerData curPlayer in mPlayerMap.Values)
				{
					Vector2Int playerPos = curPlayer.curPos;
					if (playerPos.x != curPos.x)
						continue;
					if (playerPos.y != curPos.y)
						continue;
					if (curPlayer.carryingPostIts.Count >= mMaxCarryablePostIts)
						continue;

					curPlayer.carryingPostIts.Add(curPostIt.color);
					pickedUp = true;
				}

				if (pickedUp)
				{
					mAvailablePostIts.RemoveAt(curPostItIdx);
					--curPostItIdx;
					--numPostIts;
				}
			}
		}

		bool NeedNewPostIt()
		{
			const float threshold = 8.0f;	// Stop spawning post-its if there are 8 times the amount of player

			int curAvailablePostItCount = mAvailablePostIts.Count;
			int playerCount             = mPlayerMap.Count;

			if (curAvailablePostItCount >= playerCount * threshold)
				return false;

			if (curAvailablePostItCount == 0)
				return true;

			double currentScore = Math.Pow(playerCount / (double)(curAvailablePostItCount), 4.3);
			double randomVal    = mRNG.NextDouble();

			return randomVal < currentScore;
		}

		void SpawnNewPostIt()
		{
			// Choose a random location where to spawn the next post-it
			int newX = mRNG.Next(0, (int)mGridWidth);
			int newY = mRNG.Next(0, (int)mGridHeight);
			
			// Choose a random pixel to fetch from the objective
			int objWidth  = mObjective.pixels.GetLength(0);
			int objHeight = mObjective.pixels.GetLength(1);

			int objX = mRNG.Next(0, objWidth);
			int objY = mRNG.Next(0, objHeight);

			AvailablePostIt newPostIt = new AvailablePostIt
			{
				color = mObjective.pixels[objX, objY],
				pos   = new Vector2
				{
					x = newX,
					y = newY
				}
			};

			mAvailablePostIts.Add(newPostIt);
		}

		#endregion

		#endregion
	}
}

