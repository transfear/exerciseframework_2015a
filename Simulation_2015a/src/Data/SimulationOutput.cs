﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using FrameworkInterface.Simulation;

namespace Simulation_2015a.Data
{
	public class SimulationOutput : ISimulationOutput
	{
		public class PlayerData
		{
			public FrameworkInterface.Player.IPlayer player = null;

			public float score = 0.0f;

			public Vector2Int curPos;
			public Vector2Int home;

			public bool      objChanged = false;	// has objective changed since last turn
			public Objective obj;

			public int carryingPostItCount;
			
			public PlayerData CopyDeep()
			{
				PlayerData copy = new PlayerData();

				copy.player     = player;
				copy.score      = score;
				copy.curPos     = curPos;
				copy.home       = home;
				copy.objChanged = objChanged;
				copy.obj        = obj.CopyDeep();

				copy.carryingPostItCount = carryingPostItCount;

				return copy;
			}
			
		};

		public PlayerData[] players;

		public Objective         wantedObj;
		public AvailablePostIt[] postIts;
		
		public UInt32 remainingPostIts;

		public Vector2Int gridDim;
		public UInt32 maxCarriablePostIts;

		// From ISimulationOutput interface
		public ISimulationOutput CopyDeep()
		{
			SimulationOutput copy = new SimulationOutput();

			// Copy players
			Int32 numPlayers = players.GetLength(0);
			copy.players = new PlayerData[numPlayers];
			for (Int32 curPlayerIdx = 0; curPlayerIdx < numPlayers; ++curPlayerIdx)
				copy.players[curPlayerIdx] = players[curPlayerIdx].CopyDeep();

			copy.wantedObj = wantedObj.CopyDeep();
			
			// Copy available post-its
			Int32 numPostIts = postIts.GetLength(0);
			copy.postIts = new AvailablePostIt[numPostIts];
			for (Int32 curPostItIdx = 0; curPostItIdx < numPostIts; ++curPostItIdx)
				copy.postIts[curPostItIdx] = postIts[curPostItIdx];

			copy.remainingPostIts    = remainingPostIts;
			copy.gridDim             = gridDim;
			copy.maxCarriablePostIts = maxCarriablePostIts;

			return copy;
		}
	}
}
