﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace Simulation_2015a.Data
{
	public class PlayerOutput : IPlayerOutput
	{
		public abstract class Action
		{
			public enum ActionType
			{
				Move,
				PlacePostIt,
				MovePostIt,
				DestroyPostIt
			}

			public readonly ActionType typeOfAction;

			protected Action(ActionType _type) { typeOfAction = _type; }

			public abstract Action CopyDeep();
		};

		/// <summary>
		/// Use this action to move your player around the grid.
		/// Top left corner is (0, 0) and bottom right corner
		/// is (gridWidth - 1, gridHeight - 1).
		/// </summary>
		public class MoveAction : Action
		{
			public enum Movement_e
			{
				Up = 0,
				Right,
				Down,
				Left,
				None
			};

			public Movement_e Move { get; set; }

			public MoveAction() : base(ActionType.Move) { }
			public MoveAction(Movement_e _move) : base(ActionType.Move) { Move = _move; }

			public override Action CopyDeep()
			{
				MoveAction copy = new MoveAction(Move);
				return copy;
			}
		}

		/// <summary>
		/// Use this action to place a post-it you have on yourself
		/// on your objective. If there was already a post it at this location,
		/// it's content will be overwritten.
		///
		/// You must be at home to be able to take this action.
		/// </summary>
		public class PlacePostItAction : Action 
		{
			/// <summary> X-coordinate where you want to place the post-it on your objective </summary>
			public UInt32 posX;

			/// <summary> Y-coordinate where you want to place the post-it on your objective </summary>
			public UInt32 posY;

			/// <summary> Index of the post-it to place on the object. This will reduce
			/// by 1 the amount of post-its you are carrying on yourself. </summary>
			public UInt32 postItIdx;

			public PlacePostItAction() : base(ActionType.PlacePostIt) { }
			public PlacePostItAction(UInt32 _posX, UInt32 _posY, UInt32 _postItIdx) : base(ActionType.PlacePostIt) 
			{
				posX      = _posX;
				posY      = _posY;
				postItIdx = _postItIdx;
			}

			public override Action CopyDeep()
			{
				PlacePostItAction copy = new PlacePostItAction(posX, posY, postItIdx);
				return copy;
			}
		}


		/// <summary>
		/// Use this action to move a post-it already placed on your
		/// objective from a location to another one.
		///
		/// You must be at home to be able to take this action.
		/// </summary>
		public class MovePostItAction : Action
		{
			/// <summary> X-coordinate of the post-it you want to move </summary>
			public UInt32 srcX;
			/// <summary> Y-coordinate of the post-it you want to move </summary>
			public UInt32 srcY;

			/// <summary> X-coordinate where you would like to move the post-it </summary>
			public UInt32 dstX;
			/// <summary> Y-coordinate where you would like to move the post-it </summary>
			public UInt32 dstY;

			public MovePostItAction() : base(ActionType.MovePostIt) { }
			public MovePostItAction(UInt32 _srcX, UInt32 _srcY, UInt32 _dstX, UInt32 _dstY) : base(ActionType.MovePostIt)
			{
				srcX = _srcX;
				srcY = _srcY;
				dstX = _dstX;
				dstY = _dstY;
			}

			public override Action CopyDeep()
			{
				MovePostItAction copy = new MovePostItAction(srcX, srcY, dstX, dstY);
				return copy;
			}
		}

		/// <summary>
		/// Use this action to destroy a post-it you have on yourself.
		/// Taking this action will reduce the total amount of post-its
		/// you are carrying on yourself by one.
		/// </summary>
		public class DestroyPostItAction : Action
		{
			/// <summary> Index of the post-it to destroy </summary>
			public UInt32 postItIdx; 
			
			public DestroyPostItAction() : base(ActionType.DestroyPostIt) { }
			public DestroyPostItAction(UInt32 _postItIdx) : base(ActionType.DestroyPostIt) { postItIdx = _postItIdx; }

			public override Action CopyDeep()
			{
				DestroyPostItAction copy = new DestroyPostItAction(postItIdx);
				return copy;
			}
		}

		public Action action;

		public IPlayerOutput CopyDeep()
		{
			PlayerOutput copy = new PlayerOutput();
			if (action != null)
				copy.action = action.CopyDeep();

			return copy;
		}
	}
}
