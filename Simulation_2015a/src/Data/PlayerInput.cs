﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;

namespace Simulation_2015a.Data
{
	public class PlayerInput : IPlayerInput
	{
		/// <summary> Base player data. This is available to everyone. </summary>
		public struct EnemyPlayerData
		{
			public UInt32 playerIdx;

			/// <summary> Player's position on grid </summary>
			public Vector2 pos;

			/// <summary> Player's home position on grid </summary>
			public Vector2 home;

			/// <summary> Player's current objective status </summary>
			public Objective obj;

			public EnemyPlayerData CopyDeep()
			{
				EnemyPlayerData copy = new EnemyPlayerData();

				copy.playerIdx = playerIdx;
				copy.pos       = pos;
				copy.home      = home;
				copy.obj       = obj.CopyDeep();
				
				return copy;
			}
		};

		/// <summary> Your own player data. Only available to yourself. </summary>
		public struct CurrentPlayerData
		{
			public UInt32 playerIdx;

			/// <summary> Player's position on grid </summary>
			public Vector2 pos;

			/// <summary> Player's home position on grid </summary>
			public Vector2 home;

			/// <summary> Player's current objective status </summary>
			public Objective obj;

			/// <summary> Post-its you have on yourself </summary>
			public Color[] accumulatedPostIts;

			public CurrentPlayerData CopyDeep()
			{
				CurrentPlayerData copy = new CurrentPlayerData();

				copy.playerIdx = playerIdx;
				copy.pos       = pos;
				copy.home      = home;
				copy.obj       = obj.CopyDeep();

				Int32 numPostIts = accumulatedPostIts.GetLength(0);
				copy.accumulatedPostIts = new Color[numPostIts];
				Array.Copy(accumulatedPostIts, copy.accumulatedPostIts, numPostIts);

				return copy;
			}
		}

		/// <summary> Array of enemy players </summary>
		public EnemyPlayerData[] enemies;

		/// <summary> Current player data (you!) </summary>
		public CurrentPlayerData playerData;

		/// <summary> Objective to replicate </summary>
		public Objective wantedObj;

		/// <summary> Array of post-its lying on the ground, that can be picked up by anyone </summary>
		public AvailablePostIt[] postIts;

		/// <summary> Number of remaining post-its to spawn before the game ends </summary>
		public UInt32 remainingPostIts;

		/// <summary> Width of the level </summary>
		public float gridWidth;

		/// <summary> Height of the level </summary>
		public float gridHeight;

		/// <summary> Maximum amount of post-it a player can carry </summary>
		public UInt32 maxCarriablePostIts;

		// From IPlayerInputInterface
		public IPlayerInput CopyDeep()
		{
			PlayerInput copy = new PlayerInput();

			// Copy enemy players
			Int32 numEnemies = enemies.GetLength(0);
			copy.enemies = new EnemyPlayerData[numEnemies];
			for (UInt32 curEnemyIdx = 0; curEnemyIdx < numEnemies; ++curEnemyIdx)
				copy.enemies[curEnemyIdx] = enemies[curEnemyIdx].CopyDeep();
			
			copy.playerData = playerData.CopyDeep();
			copy.wantedObj  = wantedObj.CopyDeep();

			// Copy available post its
			Int32 numPostIts = postIts.GetLength(0);
			copy.postIts = new AvailablePostIt[numPostIts];
			for (UInt32 curPostItIdx = 0; curPostItIdx < numPostIts; ++curPostItIdx)
				copy.postIts[curPostItIdx] = postIts[curPostItIdx].CopyDeep();

			copy.gridWidth  = gridWidth;
			copy.gridHeight = gridHeight;

			copy.maxCarriablePostIts = maxCarriablePostIts;
			
			return copy;
		}
	}
}
