﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simulation_2015a.Data
{
	public struct Vector2
	{
		public float x;
		public float y;

		public void Add(Vector2 _toAdd)
		{
			x += _toAdd.x;
			y += _toAdd.y;
		}

		public void Subtract(Vector2 _toSub)
		{
			x -= _toSub.x;
			y -= _toSub.y;
		}

		public void Multiply(float _multiplier)
		{
			x *= _multiplier;
			y *= _multiplier;
		}

		public void Divide(float _divider)
		{
			x /= _divider;
			y /= _divider;
		}

		public float SqrLength()
		{
			return x * x + y * y;
		}

		public float Length()
		{
			return (float)Math.Sqrt(x * x + y * y);
		}

        public float SqrDistanceTo(Vector2 _other)
        {
            _other.Subtract(this);
            return _other.SqrLength();
        }

        public float DistanceTo(Vector2 _other)
        {
            float fSqrLen = SqrDistanceTo(_other);
            return (float)Math.Sqrt(fSqrLen);
        }

		public void Normalize()
		{
			float fLen = Length();
			Divide(fLen);
		}

		public float DotProduct(Vector2 _other)
		{
			return x * _other.x + y * _other.y;
		}

		public void Rotate(float _angleRad)
		{
			float newX = (float)(x * Math.Cos(_angleRad) - y * Math.Sin(_angleRad));
			float newY = (float)(x * Math.Sin(_angleRad) + y * Math.Cos(_angleRad));

			x = newX;
			y = newY;
		}

		public Vector2Int ToVector2Int()
		{
			Vector2Int toReturn = new Vector2Int
			{
				x = (int)this.x,
				y = (int)this.y
			};

			return toReturn;
		}
	}
}
