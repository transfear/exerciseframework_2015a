﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simulation_2015a.Data
{
	/// <summary>
	/// The goal is to replicate the objective by grabbing available post-its,
	/// and placing them on your objective.
	/// </summary>
	public struct Objective
	{
		public Color[,] pixels;

		public Objective CopyDeep()
		{
			Objective copy = new Objective();

			Int32 numColumns = pixels.GetLength(0);
			Int32 numRows = pixels.GetLength(1);
			copy.pixels = new Color[numColumns, numRows];
			Array.Copy(pixels, copy.pixels, numColumns * numRows);

			return copy;
		}
	};
}
