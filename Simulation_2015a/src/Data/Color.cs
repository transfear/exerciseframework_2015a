﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simulation_2015a.Data
{
	public struct Color
	{
		public Byte R;
		public Byte G;
		public Byte B;
	}

	/// <summary>
	/// A post-it lying on the ground. Grab it before your enemies, and use
	/// it to complete your objective.
	/// </summary>
	public class AvailablePostIt
	{
		public Color color;
		public Vector2 pos;

		public AvailablePostIt CopyDeep()
		{
			AvailablePostIt copy = new AvailablePostIt();
			copy.color = color;
			copy.pos   = pos;

			return copy;
		}
	}
}
