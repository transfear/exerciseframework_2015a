﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simulation_2015a.Data
{
	public struct Vector2Int
	{
		public int x;
		public int y;

		public void Add(Vector2Int _toAdd)
		{
			x += _toAdd.x;
			y += _toAdd.y;
		}

		public void Subtract(Vector2Int _toSub)
		{
			x -= _toSub.x;
			y -= _toSub.y;
		}

		public void Multiply(int _multiplier)
		{
			x *= _multiplier;
			y *= _multiplier;
		}

		public void Divide(int _divider)
		{
			x /= _divider;
			y /= _divider;
		}

		public Vector2 ToVector2()
		{
			Vector2 toReturn = new Vector2
			{
				x = this.x,
				y = this.y
			};

			return toReturn;
		}
	}
}
