﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Simulation_2015a.Data;
using Renderer_2015a.UI;
using Renderer_2015a.Utilities;

namespace Renderer_2015a.Data
{
	public class PlayerData
	{
		public Color hue;
		public PlayerObjective uiObjective;

		public System.Windows.Controls.Image homeCtrl   = new System.Windows.Controls.Image();
		public System.Windows.Controls.Image playerCtrl = new System.Windows.Controls.Image();

		public static UInt32 GenerateRandomSeed(SimulationOutput.PlayerData[] _playerList)
		{
			IEnumerable<string> playerNameList = _playerList.Select(x => x.player.GetPlayerName());
			IEnumerable<string> dllPathList    = _playerList.Select(x => x.player.DLLPath);

			StringBuilder sb = new StringBuilder();
			foreach (string curPlayerName in playerNameList)
				sb.Append(curPlayerName);
			foreach (string curDLLPath in dllPathList)
				sb.Append(curDLLPath);

			string concatenatedString = sb.ToString();
			byte[] byteArray = Encoding.ASCII.GetBytes(concatenatedString);

			UInt32 randomSeed = MathUtils.CalculateCRC(byteArray);
			return randomSeed;
		}
	}
}
