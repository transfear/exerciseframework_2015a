﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using FrameworkInterface.Player;
using Simulation_2015a.Data;

using Renderer_2015a.Data;
using Renderer_2015a.Utilities;

namespace Renderer_2015a.UI
{
	/// <summary>
	/// Interaction logic for ExampleRendererWindow.xaml
	/// </summary>
	public partial class RendererWindow : Window
	{
		Dictionary<IPlayer, PlayerData> mPlayerMap = new Dictionary<IPlayer, PlayerData>();


		Dictionary<AvailablePostIt, System.Windows.Controls.Image> mPostItMap = new Dictionary<AvailablePostIt, System.Windows.Controls.Image>();
		BitmapImage mPostItBMP  = null;

		int mGridWidth  = 1;
		int mGridHeight = 1;
		
		Random mRNG = new Random();
		Random mDeterministicRNG = new Random();
		
		public bool CloseRequested { get; set; }

		public RendererWindow()
		{
			InitializeComponent();

			RenderOptions.SetBitmapScalingMode(imgObjective, BitmapScalingMode.NearestNeighbor);
			mPostItBMP = new BitmapImage(new Uri("pack://application:,,,/Renderer_2015a;component/rc/postit_sprite.png", UriKind.Absolute));

			CloseRequested = false;
			Closing += OnWindowClosing;
		}

		public void OnGameStart(SimulationOutput _simOutput)
		{
			// Cleanup
			mPlayerMap.Clear();
			mPostItMap.Clear();
			grdPlayers.Children.Clear();
			
			// Init base stuff
			lblRemainingPostIts.Content = _simOutput.remainingPostIts.ToString();
			SetupMainGrid(_simOutput);
			
			// Setup objective
			WriteableBitmap mainObjective = BitmapUtils.CreateWBFromObjective(_simOutput.wantedObj);
			imgObjective.Source = mainObjective;
			
			// Generate a random number generator with a fixed seed based on player input,
			// and another one, really random
			UInt32 randomSeed = PlayerData.GenerateRandomSeed(_simOutput.players);
			mDeterministicRNG = new Random((int)randomSeed);
			mRNG              = new Random();
			
			// Populate our player map
			foreach (SimulationOutput.PlayerData curPlayer in _simOutput.players)
				mPlayerMap[curPlayer.player] = new PlayerData();
			
			// Assign random color to players
			ColorUtils.AssignRandomColor(mDeterministicRNG, mPlayerMap);
			
			// Setup player grid
			foreach (SimulationOutput.PlayerData curPlayer in _simOutput.players)
			{
				PlayerData      rendererData = mPlayerMap[curPlayer.player];
				PlayerObjective uiElem       = new PlayerObjective(_simOutput.maxCarriablePostIts, curPlayer, rendererData);
				rendererData.uiObjective = uiElem;

				grdPlayers.Children.Add(uiElem);
			}

			// Setup houses
			SetupHouses(_simOutput);
			
			// Setup players
			SetupPlayers(_simOutput);

			// Position controls on main grid
			UpdateControlPosOnMainGrid(_simOutput);
		}

		public void UpdateUI(SimulationOutput _simOutput)
		{
			// Remaining post-its
			lblRemainingPostIts.Content = _simOutput.remainingPostIts.ToString();

			// Update controls position
			UpdateControlPosOnMainGrid(_simOutput);

			// Update players map
			foreach (SimulationOutput.PlayerData curPlayer in _simOutput.players)
			{
				PlayerData rendererData = mPlayerMap[curPlayer.player];
				rendererData.uiObjective.Update(curPlayer);
			}
		}

		private void OnWindowClosing(object sender, CancelEventArgs e)
		{
			// Always prevent the window from closing - let the simulation decide what to do
			CloseRequested = true;
			e.Cancel = true;
		}

		private void SetupMainGrid(SimulationOutput _simOutput)
		{
			grdMainArea.Children.Clear();

			mGridWidth  = _simOutput.gridDim.x;
			mGridHeight = _simOutput.gridDim.y;
		}

		private void SetupHouses(SimulationOutput _simOutput)
		{
			BitmapImage homeBMP = new BitmapImage(new Uri("pack://application:,,,/Renderer_2015a;component/rc/home_sprite.png", UriKind.Absolute));

			foreach (SimulationOutput.PlayerData curSimPlayerData in _simOutput.players)
			{
				IPlayer    playerInterface = curSimPlayerData.player;
				PlayerData curData         = mPlayerMap[playerInterface];
								
				WriteableBitmap wb = BitmapUtils.TintBitmap(homeBMP, curData.hue);
				curData.homeCtrl.Source = wb;

				grdMainArea.Children.Add(curData.homeCtrl);
			}
		}

		private void SetupPlayers(SimulationOutput _simOutput)
		{
			BitmapImage playerBMP = new BitmapImage(new Uri("pack://application:,,,/Renderer_2015a;component/rc/char_sprite.png", UriKind.Absolute));

			foreach (SimulationOutput.PlayerData curSimPlayerData in _simOutput.players)
			{
				IPlayer playerInterface = curSimPlayerData.player;
				PlayerData curData = mPlayerMap[playerInterface];

				WriteableBitmap wb = BitmapUtils.TintBitmap(playerBMP, curData.hue);
				curData.playerCtrl.Source = wb;

				grdMainArea.Children.Add(curData.playerCtrl);
			}
		}

		private Thickness CalcMarginFromPos(Vector2Int _pos)
		{
			double gridWidth  = grdMainArea.ActualWidth;
			double gridHeight = grdMainArea.ActualHeight;

			double cellWidth  = gridWidth / mGridWidth;
			double cellHeight = gridHeight / mGridHeight;

			double leftMargin  = cellWidth * _pos.x;
			double rightMargin = gridWidth - leftMargin - cellWidth;

			double topMargin   = cellHeight * _pos.y;
			double botMargin   = gridHeight - topMargin - cellHeight;

			Thickness toReturn = new Thickness(leftMargin, topMargin, rightMargin, botMargin);
			return toReturn;
		}

		private void UpdateControlPosOnMainGrid(SimulationOutput _simOutput)
		{
			double gridWidth  = grdMainArea.ActualWidth;
			double gridHeight = grdMainArea.ActualHeight;

			double cellWidth  = gridWidth  / mGridWidth;
			double cellHeight = gridHeight / mGridHeight;
			
			// Update all players and homes
			foreach (SimulationOutput.PlayerData curPlayer in _simOutput.players)
			{
				PlayerData curData = mPlayerMap[curPlayer.player];

				curData.homeCtrl.Width  = cellWidth;
				curData.homeCtrl.Height = cellHeight;
				curData.homeCtrl.Margin = CalcMarginFromPos(curPlayer.home);

				curData.playerCtrl.Width  = cellWidth;
				curData.playerCtrl.Height = cellHeight;
				curData.playerCtrl.Margin = CalcMarginFromPos(curPlayer.curPos);
			}
			

			// Remove all previous post-its
			KeyValuePair<AvailablePostIt, System.Windows.Controls.Image>[] toRemove = mPostItMap.Where(x => !_simOutput.postIts.Contains(x.Key)).ToArray();
			foreach (KeyValuePair<AvailablePostIt, System.Windows.Controls.Image> oldPostIt in toRemove)
			{
				grdMainArea.Children.Remove(oldPostIt.Value);
				mPostItMap.Remove(oldPostIt.Key);
			}
			
			
			// Update all new post-its
			foreach (AvailablePostIt curPostIt in _simOutput.postIts)
			{
				System.Windows.Controls.Image newPostIt = null;
				if (!mPostItMap.ContainsKey(curPostIt))
				{
					newPostIt = new System.Windows.Controls.Image();
					mPostItMap.Add(curPostIt, newPostIt);
					grdMainArea.Children.Add(newPostIt);

					WriteableBitmap wb = BitmapUtils.TintBitmap(mPostItBMP, curPostIt.color);
					newPostIt.Source = wb;
				}
				else
				{
					newPostIt = mPostItMap[curPostIt];
				}
				
				newPostIt.Width  = cellWidth;
				newPostIt.Height = cellHeight;
				newPostIt.Margin = CalcMarginFromPos(curPostIt.pos.ToVector2Int());
			}
		}
	}
}
