﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using FrameworkInterface.Player;
using Simulation_2015a.Data;

using Renderer_2015a.Data;
using Renderer_2015a.Utilities;

namespace Renderer_2015a.UI
{
	/// <summary>
	/// Interaction logic for PlayerObjective.xaml
	/// </summary>
	public partial class PlayerObjective : UserControl
	{
		private readonly PlayerData mRendererData;
		private readonly uint muiMaxCarryingPostIt;

		public PlayerObjective(uint _uiMaxPostIt, SimulationOutput.PlayerData _simData, PlayerData _rendererData)
		{
			mRendererData        = _rendererData;
			muiMaxCarryingPostIt = _uiMaxPostIt;

			InitializeComponent();
			RenderOptions.SetBitmapScalingMode(imgObjective, BitmapScalingMode.NearestNeighbor);

			Init(_simData);
		}


		private void Init(SimulationOutput.PlayerData _simData)
		{
			// Setup labels
			lblPlayerName.Content = _simData.player.GetPlayerName();
			lblPlayerName.Foreground = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, mRendererData.hue.R, mRendererData.hue.G, mRendererData.hue.B));
			
			// Setup objective
			WriteableBitmap mainObjective = BitmapUtils.CreateWBFromObjective(_simData.obj);
			imgObjective.Source = mainObjective;

			UpdateControls(_simData);
		}

		public void Update(SimulationOutput.PlayerData _simData)
		{
			// Update objective if requested
			if (_simData.objChanged)
			{
				WriteableBitmap mainObjective = BitmapUtils.CreateWBFromObjective(_simData.obj);
				imgObjective.Source = mainObjective;
			}

			// Update other controls
			UpdateControls(_simData);
		}

		private void UpdateControls(SimulationOutput.PlayerData _simData)
		{
			// Update score
			lblScore.Content = _simData.score.ToString();

			// Update carrying post it count
			lblPostIts.Content = _simData.carryingPostItCount.ToString();
			float fPostItFraction = (float)_simData.carryingPostItCount / (float)muiMaxCarryingPostIt;

			// Update background brush
			LinearGradientBrush backgroundBrush = new LinearGradientBrush();
			backgroundBrush.GradientStops.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 170, 170, 170), 0.0));
			backgroundBrush.GradientStops.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 170, 170, 170), fPostItFraction));
			backgroundBrush.GradientStops.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 235, 235, 235), fPostItFraction));
			backgroundBrush.GradientStops.Add(new GradientStop(System.Windows.Media.Color.FromArgb(255, 235, 235, 235), 1.0));
			stckPnlPostIts.Background = backgroundBrush;
		}
	}
}
