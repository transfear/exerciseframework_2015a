﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Simulation_2015a.Data;

namespace Renderer_2015a.Utilities
{
	static class BitmapUtils
	{
		public static WriteableBitmap CreateWBFromObjective(Objective _obj)
		{
			int width  = _obj.pixels.GetLength(0);
			int height = _obj.pixels.GetLength(1);

			int bufSize = width * height * 3;
			byte[] pixelBuf = new byte[bufSize];

			for (int curRow = 0; curRow < height; ++curRow)
			{
				int iRowStart = curRow * width * 3;

				// Loop on all columns of this row
				for (int curCol = 0; curCol < width; ++curCol)
				{
					int iByteOffset = iRowStart + curCol * 3;
					pixelBuf[iByteOffset + 0] = _obj.pixels[curCol, curRow].R;
					pixelBuf[iByteOffset + 1] = _obj.pixels[curCol, curRow].G;
					pixelBuf[iByteOffset + 2] = _obj.pixels[curCol, curRow].B;
				}
			}

			WriteableBitmap wb = new WriteableBitmap(width, height, 96, 96, PixelFormats.Rgb24, null);
			wb.WritePixels(new Int32Rect(0, 0, width, height), pixelBuf, width * 3, 0);

			return wb;
		}

		public static WriteableBitmap TintBitmap(BitmapImage _bmp, Simulation_2015a.Data.Color _tint)
		{
			double R = (double)_tint.R / 255.0;
			double G = (double)_tint.G / 255.0;
			double B = (double)_tint.B / 255.0;

			// Convert to 96 DPI
			double   dpi  = 96.0;
			int imgWidth  = _bmp.PixelWidth;
			int imgHeight = _bmp.PixelHeight;

			int iStride = imgWidth * _bmp.Format.BitsPerPixel;
			byte[] pixelData = new byte[iStride * imgHeight];
			_bmp.CopyPixels(pixelData, iStride, 0);

			BitmapSource bs = BitmapSource.Create(imgWidth, imgHeight, dpi, dpi, _bmp.Format, null, pixelData, iStride);
			
			// Copy to our objective internal color array
			if (bs.Format != PixelFormats.Bgra32)
				bs = new FormatConvertedBitmap(bs, PixelFormats.Bgra32, null, 0);
			
			// Tint our image
			int numBytes = imgWidth * imgHeight * 4;
			byte[] tmpBuf = new byte[numBytes];
			bs.CopyPixels(tmpBuf, imgWidth * 4, 0);

			int byteOffset = 0;
			for (int curY = 0; curY < imgHeight; ++curY)
			{
				for (int curX = 0; curX < imgWidth; ++curX)
				{
					// B
					tmpBuf[byteOffset] = (byte)((double)tmpBuf[byteOffset] * B);
					++byteOffset;

					// G
					tmpBuf[byteOffset] = (byte)((double)tmpBuf[byteOffset] * G);
					++byteOffset;

					// R
					tmpBuf[byteOffset] = (byte)((double)tmpBuf[byteOffset] * R);
					++byteOffset;

					// A
					++byteOffset;
				}
			}

			WriteableBitmap wb = new WriteableBitmap(imgWidth, imgHeight, 96.0, 96.0, PixelFormats.Bgra32, null);
			wb.WritePixels(new Int32Rect(0, 0, imgWidth, imgHeight), tmpBuf, imgWidth * 4, 0);

			return wb;
		}
	}
}
