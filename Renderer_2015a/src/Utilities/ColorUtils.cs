﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FrameworkInterface.Player;
using Simulation_2015a.Data;

using Renderer_2015a.Data;

namespace Renderer_2015a.Utilities
{
	static class ColorUtils
	{
		// Given H,S,L in range of 0-1
		// Returns a Color (RGB struct) in range of 0-255
		public static Color HSLToRGB(double _hue, double _saturation, double _luminance)
		{

			double r = _luminance;   // default to gray
			double g = _luminance;
			double b = _luminance;

			double v = (_luminance <= 0.5) ? (_luminance * (1.0 + _saturation)) : (_luminance + _saturation - _luminance * _saturation);

			if (v > 0)
			{
				double m  = _luminance + _luminance - v;
				double sv = (v - m) / v;

				_hue *= 6.0;

				int sextant = (int)_hue;

				double fract = _hue - sextant;
				double vsf   = v * sv * fract;
				double mid1  = m + vsf;
				double mid2  = v - vsf;

				switch (sextant)
				{
					case 0:
					{
						r = v;
						g = mid1;
						b = m;
						break;
					}

					case 1:
					{
						r = mid2;
						g = v;
						b = m;
						break;
					}

					case 2:
					{
						r = m;
						g = v;
						b = mid1;
						break;
					}

					case 3:
					{
						r = m;
						g = mid2;
						b = v;
						break;
					}

					case 4:
					{
						r = mid1;
						g = m;
						b = v;
						break;
					}

					case 5:
					{
						r = v;
						g = m;
						b = mid2;
						break;
					}
				}
			}

			Color rgb;
			rgb.R = Convert.ToByte(r * 255.0f);
			rgb.G = Convert.ToByte(g * 255.0f);
			rgb.B = Convert.ToByte(b * 255.0f);

			return rgb;

		}


		public static void AssignRandomColor(Random _rng, Dictionary<IPlayer, PlayerData> _players)
		{
			double baseOffset = _rng.NextDouble();

			int    numPlayers      = _players.Count;
			double perPlayerOffset = 1.0 / (double)numPlayers;

			double curHue = baseOffset;
			foreach (PlayerData curData in _players.Values)
			{
				curData.hue = HSLToRGB(curHue, 1.0, 0.5);
				
				curHue += perPlayerOffset;
				if (curHue > 1.0)
					curHue -= 1.0;
			}
		}
	}
}
