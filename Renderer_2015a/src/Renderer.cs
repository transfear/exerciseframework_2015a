﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

using Simulation_2015a.Data;
using FrameworkInterface.Renderer;
using FrameworkInterface.Simulation;

namespace Renderer_2015a
{
	public class ExampleRenderer : IRenderer
	{
		AutoResetEvent    mWindowSetEvent = new AutoResetEvent(false);
		UI.RendererWindow mWindow         = null;
		UIThread          mUIThread       = null;

		public IRendererFeedback OnGameStart(ISimulationOutput _simulationData)
		{
			SimulationOutput simOutput = _simulationData as SimulationOutput;
			if (simOutput == null)
				return null;

			bool firstStart = mUIThread == null;
			if (firstStart)
			{
				mUIThread = new UIThread(this);
				mUIThread.StartUIThread(simOutput);
				mWindowSetEvent.WaitOne();
			}
			
			mWindow.CloseRequested = false;
			mWindow.Dispatcher.BeginInvoke((Action)(() => { mWindow.OnGameStart(simOutput); }));

			return null;
		}

		public void SetWindow(UI.RendererWindow _window)
		{
			mWindow = _window;
			mWindowSetEvent.Set();
		}

		public IRendererFeedback Update(ISimulationOutput _simulationData)
		{
			SimulationOutput simOutput = _simulationData as SimulationOutput;
			if (simOutput == null)
				return null;

			// Create feedback for the simulation
			RendererFeedback feedback = new RendererFeedback();
			feedback.ShouldQuit = false;

			if (mWindow != null)
			{
				feedback.ShouldQuit    = mWindow.CloseRequested;
				feedback.ShouldRestart = false;

				mWindow.Dispatcher.BeginInvoke((Action)(() => { mWindow.UpdateUI(simOutput); }));				
			}

			return feedback;
		}

		public IRendererFeedback OnGameEnd(ISimulationOutput _simulationData)
		{
			RendererFeedback newFeedBack = new RendererFeedback();
			newFeedBack.ShouldQuit    = true;
			newFeedBack.ShouldRestart = false;

			SimulationOutput simOutput = _simulationData as SimulationOutput;
			if (simOutput != null)
			{
				// Attempt to find best player
				float bestScore = -1.0f;
				SimulationOutput.PlayerData bestPlayer = null;
				foreach (SimulationOutput.PlayerData curPlayer in simOutput.players)
				{
					if (curPlayer.score > bestScore)
					{
						bestScore = curPlayer.score;
						bestPlayer = curPlayer;
					}
				}

				if (bestPlayer != null)
				{
					MessageBoxResult msgResult = MessageBox.Show(bestPlayer.player.GetPlayerName() + " won with " + bestScore.ToString("N2") + " points. Would you like to restart?", "Game finished!", MessageBoxButton.YesNo);
					newFeedBack.ShouldRestart = msgResult == MessageBoxResult.Yes;
				}
			}

			// Close the window
			mWindow.Dispatcher.Invoke((Action)(() => 
			{ 
				// Explicit shutdown
				if (!newFeedBack.ShouldRestart)
				{
					mWindow.Close();
					mUIThread.ExplicitShutdown();
				}
			}));

			// Wait for the thread to finish
			if (!newFeedBack.ShouldRestart)
				mUIThread.Join();

			return newFeedBack;
		}
	}
}
