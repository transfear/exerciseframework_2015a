﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

using Simulation_2015a.Data;

namespace Renderer_2015a
{
	internal class UIThread
	{
		private Thread            mThread      = null;
		private Application       mApplication = null;
		private UI.RendererWindow mWindow      = null;
		private ExampleRenderer   mParent      = null;
		private SimulationOutput  mSimOutput   = null;

		public UIThread(ExampleRenderer _parent)
		{
			mParent = _parent;

			mThread = new Thread(new ThreadStart(this.ThreadedRun));
			mThread.SetApartmentState(ApartmentState.STA);
		}

		public void StartUIThread(SimulationOutput _simOutput)
		{
			mSimOutput = _simOutput;
			mThread.Start();
		}

		public void ExplicitShutdown()
		{
			if (mApplication != null)
				mApplication.Shutdown();
		}

		public void Join()
		{
			mThread.Join();
		}

		private void ThreadedRun()
		{
			mWindow = new UI.RendererWindow();
			mParent.SetWindow(mWindow);
			
			if (mApplication == null)
			{
				mApplication = new Application();
				mApplication.ShutdownMode = ShutdownMode.OnExplicitShutdown;
			}
			mApplication.Run(mWindow);
		}
	}
}
