﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Simulation_2015a.Data;
using FrameworkInterface.Player;

namespace RandomPlayer
{
	public class QuickNDumb : IPlayer
	{
		enum MyStates
		{
			kState_GoToPostIt,
			kState_ReturnHome,
			kState_PlacePostIt
		}

		MyStates mCurrentState;
		int      miNumPostItPlaced;

		public QuickNDumb() { }

		public override string GetPlayerName()
		{
			return "QuickNDumb";
		}

		public override IPlayerOutput OnGameStart(IPlayerInput _input)
		{
			mCurrentState = MyStates.kState_GoToPostIt;
			miNumPostItPlaced = 0;
			
			return null;
		}

		public override IPlayerOutput Update(IPlayerInput _input)
		{
			PlayerInput input = _input as PlayerInput;

			switch (mCurrentState)
			{
				case MyStates.kState_GoToPostIt:
					return Update_GoToPostIt(input);

				case MyStates.kState_ReturnHome:
					return Update_ReturnHome(input);

				case MyStates.kState_PlacePostIt:
					return Update_PlacePostIt(input);
			}
			
			return null;
		}

		public override IPlayerOutput OnGameEnd(IPlayerInput _input)
		{
			return null;
		}

		#region Utilities
		private AvailablePostIt FindClosestPostIt(PlayerInput _input, Vector2 fromPos, out float fOutDistance)
		{
			AvailablePostIt closestPostIt = null;

			float fClosestDistance = float.MaxValue;
			foreach (AvailablePostIt curPostIt in _input.postIts)
			{
				float fDistance = Math.Abs(curPostIt.pos.x - fromPos.x) + Math.Abs(curPostIt.pos.y - fromPos.y);
				if (fDistance < fClosestDistance)
				{
					fClosestDistance = fDistance;
					closestPostIt = curPostIt;
				}
			}

			fOutDistance = fClosestDistance;
			return closestPostIt;
		}
		#endregion

		#region StateUpdates
		private PlayerOutput Update_GoToPostIt(PlayerInput _input)
		{
			// Find the closest post-it (quick)
			Vector2         myPos         = _input.playerData.pos;
			float           fDistance     = 0.0f;
			AvailablePostIt closestPostIt = FindClosestPostIt(_input, myPos, out fDistance);

			// Choose which direction to go
			PlayerOutput output = new PlayerOutput();
			if (closestPostIt != null)
			{
				if (myPos.x < closestPostIt.pos.x)
				{
					output.action = new PlayerOutput.MoveAction(PlayerOutput.MoveAction.Movement_e.Right);
				}
				else if (myPos.x > closestPostIt.pos.x)
				{
					output.action = new PlayerOutput.MoveAction(PlayerOutput.MoveAction.Movement_e.Left);
				}
				else if (myPos.y < closestPostIt.pos.y)
				{
					output.action = new PlayerOutput.MoveAction(PlayerOutput.MoveAction.Movement_e.Down);
				}
				else
				{
					output.action = new PlayerOutput.MoveAction(PlayerOutput.MoveAction.Movement_e.Up);
				}
			}

			// Return to home once we've accumulated max amount of post its, or none are remaining
			
			bool bReturnHome = fDistance < 1.5f;
			bReturnHome = bReturnHome && _input.playerData.accumulatedPostIts.Count() == (_input.maxCarriablePostIts - 1);
			bReturnHome = bReturnHome || (closestPostIt == null);

			if (bReturnHome)
				mCurrentState = MyStates.kState_ReturnHome;

			return output;
		}


		private PlayerOutput Update_ReturnHome(PlayerInput _input)
		{
			Vector2 vHome = _input.playerData.home;
			Vector2 myPos = _input.playerData.pos;

			// Choose which direction to go
			PlayerOutput output = new PlayerOutput();

			if (myPos.x < vHome.x)
			{
				output.action = new PlayerOutput.MoveAction(PlayerOutput.MoveAction.Movement_e.Right);
			}
			else if (myPos.x > vHome.x)
			{
				output.action = new PlayerOutput.MoveAction(PlayerOutput.MoveAction.Movement_e.Left);
			}
			else if (myPos.y < vHome.y)
			{
				output.action = new PlayerOutput.MoveAction(PlayerOutput.MoveAction.Movement_e.Down);
			}
			else
			{
				output.action = new PlayerOutput.MoveAction(PlayerOutput.MoveAction.Movement_e.Up);
			}
			
			// If next move will take us home, change state
			float fDistance = Math.Abs(vHome.x - myPos.x) + Math.Abs(vHome.y - myPos.y);
			if (fDistance < 1.5f)
				mCurrentState = MyStates.kState_PlacePostIt;
			
			return output;
		}

		private PlayerOutput Update_PlacePostIt(PlayerInput _input)
		{
			// Place the post-it sequentially on the image (dumb)
			int objectiveWidth  = _input.wantedObj.pixels.GetLength(0);
			int objectiveHeight = _input.wantedObj.pixels.GetLength(1);
			int totalPixelCount = objectiveWidth * objectiveHeight;

			int indexInImage = miNumPostItPlaced % totalPixelCount;
			int iColumn      = indexInImage % objectiveWidth;
			int iRow         = indexInImage / objectiveWidth;

			// Always take the first post it
			PlayerOutput output = new PlayerOutput();
			output.action = new PlayerOutput.PlacePostItAction((UInt32)iColumn, (UInt32)iRow, 0);
			++miNumPostItPlaced;

			// If we were only carrying one post-it, our next action will be to fetch another one
			if (_input.playerData.accumulatedPostIts.Count() < 2)
				mCurrentState = MyStates.kState_GoToPostIt;

			return output;
		}
		#endregion
	}
}
