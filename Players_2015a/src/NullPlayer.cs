﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Simulation_2015a.Data;
using FrameworkInterface.Player;

namespace RandomPlayer
{
	public class NullPlayer : IPlayer
	{
		public NullPlayer() { }

		public override string GetPlayerName()
		{
			return "Null player";
		}

		public override IPlayerOutput OnGameStart(IPlayerInput _input)
		{
			return null;
		}

		public override IPlayerOutput Update(IPlayerInput _input)
		{
			return null;
		}

		public override IPlayerOutput OnGameEnd(IPlayerInput _input)
		{
			return null;
		}
	}
}
