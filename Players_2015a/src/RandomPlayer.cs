﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Simulation_2015a.Data;
using FrameworkInterface.Player;

namespace RandomPlayer
{
	public class RandomPlayer : IPlayer
	{
		Random mRNG = new Random();

		public RandomPlayer() { }

		public override string GetPlayerName()
		{
			return "Random player";
		}

		public override IPlayerOutput OnGameStart(IPlayerInput _input)
		{
			return null;
		}

		public override IPlayerOutput Update(IPlayerInput _input)
		{
			PlayerOutput output = new PlayerOutput();

			PlayerOutput.MoveAction.Movement_e where = (PlayerOutput.MoveAction.Movement_e)mRNG.Next(0, (int)PlayerOutput.MoveAction.Movement_e.None + 1);
			output.action = new PlayerOutput.MoveAction(where);

			return output;
		}

		public override IPlayerOutput OnGameEnd(IPlayerInput _input)
		{
			return null;
		}
	}
}
